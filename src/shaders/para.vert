// attributes of our mesh
in float position;
in float angle;
in vec2 uv;

// built-in uniforms from ThreeJS camera and Object3D
uniform mat4 projectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;
uniform mat4 modelMatrix;

// custom uniforms to build up our tubes
uniform float thickness;
uniform float time;
uniform float animateRadius;
uniform float animateStrength;
uniform float index;
uniform float radialSegments;

// pass a few things along to the vertex shader
out vec2 vUv;
out vec3 vViewPosition;
out vec3 vNormal;
out float vThickness;

#define PI 3.14149


const float lengthSegments = 200.0;
// Import a couple utilities
#define PI 3.14149
float ease(float t) {
  return t == 0.0 || t == 1.0
    ? t
    : t < 0.5
      ? +0.5 * pow(2.0, (20.0 * t) - 10.0)
      : -0.5 * pow(2.0, 10.0 - (t * 20.0)) + 1.0;
}

// Angles to spherical coordinates
vec3 spherical (float r, float phi, float theta) {
  return r * vec3(
    cos(phi) * cos(theta),
    cos(phi) * sin(theta),
    sin(phi)
  );
}


// Creates an animated torus knot
vec3 sampleCurve(float t) {
  float beta = t * PI;

  float ripple = ease(sin(t * 2.0 * PI + time) * 0.5 + 0.5) * 0.5;
  float noise = time + index * ripple * 8.0;

  // animate radius on click
  float radiusAnimation = animateRadius * animateStrength * 0.25;
  float r = sin(index * 0.75 + beta * 2.0) * (0.75 + radiusAnimation);
  float theta = 4.0 * beta + index * 0.25;
  float phi = sin(index * 2.0 + beta * 8.0 + noise);
    vec3 val = spherical(r, phi, theta);
  return val;
}

// ------
// Fast version; computes the local Frenet-Serret frame
// ------
void createTube (float t, vec2 volume, out vec3 offset, out vec3 normal) {
  // find next sample along curve
  float nextT = t + (1.0 / lengthSegments);

  // sample the curve in two places
  vec3 current = sampleCurve(t);
  vec3 next = sampleCurve(nextT);

  // compute the TBN matrix
  vec3 T = normalize(next - current);
  vec3 B = normalize(cross(T, next + current));
  vec3 N = -normalize(cross(B, T));

  // extrude outward to create a tube
  float tubeAngle = angle;
  float circX = cos(tubeAngle);
  float circY = sin(tubeAngle);

  // compute position and normal
  normal.xyz = normalize(B * circX + N * circY);
  offset.xyz = current + B * volume.x * circX + N * volume.y * circY;
}


void main() {
  // current position to sample at
  // [-0.5 .. 0.5] to [0.0 .. 1.0]
  float t = (position * 2.0) * 0.5 + 0.5;

  // build our tube geometry
  vec2 volume = vec2(thickness);

  // animate the per-vertex curve thickness
  float volumeAngle = t * lengthSegments * 0.5 + index * 20.0 + time;
  float volumeMod = sin(volumeAngle) * 0.5 + 0.5;
  volume += 0.01 * volumeMod;

  // build our geometry
  vec3 transformed;
  vec3 objectNormal;
  createTube(t, volume, transformed, objectNormal);

  // pass the normal and UV along
  vec3 transformedNormal = normalMatrix * objectNormal;
  vNormal = normalize(transformedNormal);
  vUv = uv.yx; // swizzle this to match expectations

  // scale vertices up a bit
  transformed.x *= 5.0;
  transformed.y *= 5.0;
  transformed.z *= 5.0;

  // project our vertex position
  vec4 mvPosition = modelMatrix * modelViewMatrix * vec4(transformed, 1.0);
  vViewPosition = -mvPosition.xyz;
  gl_Position = projectionMatrix * mvPosition;
}