import {createRenderer} from './jirachi/core/gl'
import {createPerspectiveCamera,setZoom,fullscreenAspectRatio,updateAspectRatio} from './jirachi/framework/camera'
import Loader from './jirachi/loader'
import ObjLoader from './jirachi/parsers/obj'
import Scene from './Boot'
import {createFBO} from './jirachi/core/fbo'
import {createTexture2d} from './jirachi/core/texture'
import DrawQuad from './jirachi/drawquad'

let gl = createRenderer()
	.setFullscreen()
	.attachToScreen();

let camera = createPerspectiveCamera(45 * (Math.PI / 10),fullscreenAspectRatio(),0.1,100);
camera = setZoom(camera,-10)

let scene = new Scene(gl,camera);

// ===================
let loader = new Loader();

loader.loadItem("./models/petal.obj","text")
	.then(itm => {

		// petal model content
		// TODO inline content?
		let content = ObjLoader.parseObj(itm);

		// setup the scene
		scene.setup(content);

		// kick off animation
		animate();
	});

window.addEventListener('resize',()=>{
	camera = updateAspectRatio(camera,fullscreenAspectRatio())
})

function animate(){
	requestAnimationFrame(animate);
	scene.draw();
}
