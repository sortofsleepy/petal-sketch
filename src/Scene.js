import Petals from './objects/Petals'
import DataView from './objects/DataView'
import DrawQuad from './jirachi/drawquad'

// shaders
import sim from './shaders/sim.frag'

class Scene {
	constructor(gl,camera){

		this.gl = gl;
		this.camera = camera;
		this.count = 0;
		let numParticles = 100;
		this.time = 0.0;


		this.numParticles = numParticles;
	}

	setup(content){
		let gl = this.gl;
		this.quad = new DrawQuad(gl);
		this.data = new DataView(gl,100);
		this.sim = new DrawQuad(gl,{
			fragment:sim,
			uniforms:[
				'texturePos',
				'textureVel',
				'textureExtra',
				'maxRadius'
			]
		});

		this.petals = new Petals(this.gl,content,this.numParticles)
		this.petals.setNumInstances(this.numParticles);
		this.current = this.data.buffers[0];
		this.target = this.data.buffers[1];

	}
	updateFbo(){
		let gl = this.gl;

		/*
		 textureVel = 1,
		 texturePos = 0,
		 textureExtra = 2
		 */
		this.target.bind();
		gl.clearScreen();
		this.current.getTexture(1).bind(0);
		this.current.getTexture().bind(1);
		this.current.getTexture(2).bind(2);
		gl.setViewport(0,0,this.numParticles,this.numParticles);
		this.sim.draw(shader => {
			shader.setTextureUniform('texturePos',1);
			shader.setTextureUniform('textureVel',0);
			shader.setTextureUniform('textureExtra',2);
			shader.uniform('time',this.time)
			shader.uniform('maxRadius',40.5);
		})

		gl.bindTexture(gl.TEXTURE_2D,null);
		this.target.unbind();
		let tmp = this.current;
		this.current = this.target;
		this.target = tmp;
	}
	draw(){
		let gl = this.gl;
		this.time += 0.05;
		this.count ++;
		if(this.count % 2 == 0) {
			this.count = 0;

			this.updateFbo();
		}



		/*
		 textureCurr = 0
		textureNext = 0
		textureExtra = 2
		 */
		gl.clearScreen()
		gl.setViewport();

		this.target.getTexture().bind(0);
		this.target.getTexture().bind(1);
		this.target.getTexture(2).bind(2);
		this.petals.draw(this.camera,(shader)=>{


			shader.setTextureUniform("textureCurr",0);
			shader.setTextureUniform('textureNext',1);
			shader.setTextureUniform('textureExtra',2);
			shader.uniform('percent',this.count)
			shader.uniform('time',this.time * 0.001);
			shader.uniform('scale',1.0);
		});

		gl.bindTexture(gl.TEXTURE_2D,null);
		//this.quad.drawTexture(this.target.getTexture(3));

	}
}

export default Scene;