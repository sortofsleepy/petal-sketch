precision highp float;
uniform float sample_offset;
uniform sampler2D inputTexture;
const float attenuation = 0.04323;
in vec2 vUv;
out vec4 glFragColor;
void main(){
     vec3 sum = vec3( 0.0, 0.0, 0.0 );
      vec4 texDat = texture( inputTexture, vUv );

     sum += texture( inputTexture, vUv + -10.0 * sample_offset ).rgb * 0.009167927656011385;
     sum += texture( inputTexture, vUv +  -9.0 * sample_offset ).rgb * 0.014053461291849008;
     sum += texture( inputTexture, vUv +  -8.0 * sample_offset ).rgb * 0.020595286319257878;
     sum += texture( inputTexture, vUv +  -7.0 * sample_offset ).rgb * 0.028855245532226279;
     sum += texture( inputTexture, vUv +  -6.0 * sample_offset ).rgb * 0.038650411513543079;
     sum += texture( inputTexture, vUv +  -5.0 * sample_offset ).rgb * 0.049494378859311142;
     sum += texture( inputTexture, vUv +  -4.0 * sample_offset ).rgb * 0.060594058578763078;
     sum += texture( inputTexture, vUv +  -3.0 * sample_offset ).rgb * 0.070921288047096992;
     sum += texture( inputTexture, vUv +  -2.0 * sample_offset ).rgb * 0.079358891804948081;
     sum += texture( inputTexture, vUv +  -1.0 * sample_offset ).rgb * 0.084895951965930902;
     sum += texture( inputTexture, vUv +   0.0 * sample_offset ).rgb * 0.086826196862124602;
     sum += texture( inputTexture, vUv +  +1.0 * sample_offset ).rgb * 0.084895951965930902;
     sum += texture( inputTexture, vUv +  +2.0 * sample_offset ).rgb * 0.079358891804948081;
     sum += texture( inputTexture, vUv +  +3.0 * sample_offset ).rgb * 0.070921288047096992;
     sum += texture( inputTexture, vUv +  +4.0 * sample_offset ).rgb * 0.060594058578763078;
     sum += texture( inputTexture, vUv +  +5.0 * sample_offset ).rgb * 0.049494378859311142;
     sum += texture( inputTexture, vUv +  +6.0 * sample_offset ).rgb * 0.038650411513543079;
     sum += texture( inputTexture, vUv +  +7.0 * sample_offset ).rgb * 0.028855245532226279;
     sum += texture( inputTexture, vUv +  +8.0 * sample_offset ).rgb * 0.020595286319257878;
     sum += texture( inputTexture, vUv +  +9.0 * sample_offset ).rgb * 0.014053461291849008;
     sum += texture( inputTexture, vUv + +10.0 * sample_offset ).rgb * 0.009167927656011385;

     //glFragColor = vec4(texDat.xyz,1.0);
     glFragColor = vec4(attenuation * sum * 10000.0,1.0);
    // glFragColor = vec4(sum.xy,0.0,1.0);
}