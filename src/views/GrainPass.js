import View from '../jirachi/framework/View'
import grain from '../jirachi/shaders/post/grain.glsl'
class GrainPass extends View {
	constructor(gl){
		super(gl,{
			fragment:grain,
			uniforms:[
				'time',
				'nIntensity'
			]
		});
	}

	runPass(){
		this.fbo.bind();
		this.gl.clearScreen();
		if(this.input !== null){
			this.input.bind();
		}
		this.drawQuad.drawTexture(this.input,(shader)=>{
			shader.uniform("time",deltaTime);
			shader.uniform("nIntensity",0.4)
		});
		this.fbo.unbind();
	}
}

export default GrainPass;