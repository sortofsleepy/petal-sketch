import View from '../jirachi/framework/View'
import Parametric from '../objects/Parametric'
class ShapeView extends View {
    constructor(gl,camera){
        super(gl);

        this.shape = new Parametric(gl,{
            numSides:8,
            subdivisions:300
        });

        this.camera = camera;
    }

    drawShapes(){
        let camera = this.camera;

        let gl =  this.gl;

        this.bindView()
        this.shape.draw(camera);
        this.unbindView()

        gl.clearScreen()


        this.drawQuad.drawTexture(this.getTexture())
    }
}

export default ShapeView;