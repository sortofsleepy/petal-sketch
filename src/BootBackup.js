import Petals from './objects/Petals'
import Parametric from './objects/Parametric'
import DataView from './objects/DataView'
import DrawQuad from './jirachi/drawquad'
import {loadContextConditionalShader} from './jirachi/core/shader'

// post
import SceneView from './Composer'
import BloomView from './views/BloomView'
import BlurView from './views/BlurPass'

// shaders
import sim from './shaders/sim.frag'
import sim1 from './shaders/es1/sim.frag'

import PetalView from './views/PetalView'
import CompositeView from './views/CompositeView'

class Boot {
	constructor(gl,camera){

		this.gl = gl;
		this.camera = camera;
		this.count = 0;
		let numParticles = 100;
		this.time = 0.0;

		this.numParticles = numParticles;
	}

	setup(content){
		let gl = this.gl;
		this.quad = new DrawQuad(gl);
		this.data = new DataView(gl,100);
		this.sim = new DrawQuad(gl,{
			fragment:loadContextConditionalShader(gl,{
				webgl1:sim1,
				webgl2:sim
			}),
			uniforms:[
				'texturePos',
				'textureVel',
				'textureExtra',
				'maxRadius'
			]
		});

		this.petals = new Petals(this.gl,content,this.numParticles);
		this.shape = new Parametric(gl,{
			numSides:10,
			subdivisions:1000,
			openEnded:false
		});
		this.petals.setNumInstances(this.numParticles);
		this.current = this.data.buffers[0];
		this.target = this.data.buffers[1];
		this.petalScene = new PetalView(this.gl);


		/**
		 * Just cause I'm lazy - post processing only for WebGL2 for now.
		 */
		if(this.gl instanceof WebGL2RenderingContext){
			this.bloom = new BloomView(this.gl);
			this.blur = new BlurView(gl);

			this.petalScene.addLayer(this.bloom,this.blur);
			this.petalScene.compileLayers();
		}

		window.addEventListener('resize',() => {
			this.petalScene.fbo.resize(window.innerWidth,window.innerHeight);
			this.blur.fbo.resize(window.innerWidth,window.innerHeight);
			gl.viewport(0,0,window.innerWidth,window.innerHeight)
		})

	}
	updateFbo(){
		let gl = this.gl;
		this.target.bind();

		gl.clearScreen();

		this.current.getTexture(1).bind(0);
		this.current.getTexture().bind(1);
		this.current.getTexture(2).bind(2);
		gl.setViewport(0,0,this.numParticles,this.numParticles);
		this.sim.draw(shader => {
			shader.setTextureUniform('texturePos',1);
			shader.setTextureUniform('textureVel',0);
			shader.setTextureUniform('textureExtra',2);
			shader.uniform('time',this.time)
			shader.uniform('maxRadius',40.5);
		})

		gl.bindTexture(gl.TEXTURE_2D,null);

		this.target.unbind();

		let tmp = this.current;
		this.current = this.target;
		this.target = tmp;
	}

	/**
	 * Renders the blurred petals
	 */
	renderPetals(){
		let gl = this.gl;
		this.time += 0.05;
		this.count ++;

		if(this.count % 2 == 0) {
			this.count = 0;
			this.updateFbo();
		}

		this.petalScene.bindView();
		gl.clearScreen()
		gl.setViewport();
		gl.enable(gl.BLEND);
		gl.setBlendFunction("SRC_ALPHA","ONE");

		this.target.getTexture().bind(0);
		this.target.getTexture().bind(1);
		this.target.getTexture(2).bind(2);
		this.petals.draw(this.camera,(shader)=>{

			shader.setTextureUniform("textureCurr",0);
			shader.setTextureUniform('textureNext',1);
			shader.setTextureUniform('textureExtra',2);
			shader.uniform('percent',this.count)
			shader.uniform('time',this.time * 0.01);
			shader.uniform('scale',1.0);
		});

		gl.disableBlending();
		gl.enable(gl.DEPTH_TEST);
		gl.bindTexture(gl.TEXTURE_2D,null);

		this.blur.unbindView();

	}

	draw(){
		let gl = this.gl;
		this.renderPetals();
		gl.setViewport();


		if(this.gl instanceof WebGL2RenderingContext) {
			//this.petalScene.draw();
			this.petalScene.run();
			this.petalScene.drawComposedScene();

		}else{
			this.petalScene.draw();
		}


	}
}

export default Boot;