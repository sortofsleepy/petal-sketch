import Mesh from '../jirachi/framework/Mesh'
import createPlane from 'primitive-plane'

import vert from '../shaders/debug.vert'
import frag from '../shaders/debug.frag'
import {flattenArray} from '../jirachi/math/core'
class DebugPlane extends Mesh {
	constructor(gl){
		super(gl,{
			vertex:vert,
			fragment:frag,
			uniforms:[
				'inputTex'
			]
		});

		let plane = createPlane(400,400,3,3);


		this.addAttribute('position',plane.positions);
		this.addAttribute('uv',flattenArray(plane.uvs),2);
		this.addIndices(flattenArray(plane.cells));

	}
}

export default DebugPlane;