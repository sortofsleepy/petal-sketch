import Mesh from '../jirachi/framework/Mesh'
import {loadContextConditionalShader} from '../jirachi/core/shader'
import vert from '../shaders/petal.vert'
import frag from '../shaders/petal.frag'
import vert1 from '../shaders/es1/petal.vert'
import frag1 from '../shaders/es1/petal.frag'

// Adapted from http://yiwenl.github.io/Sketches/exps/32
// Basically the same thing.
class Petals extends Mesh {
	constructor(gl,model,numParticles){
		super(gl,{
			vertex:loadContextConditionalShader(gl,{
				webgl1:vert1,
				webgl2:vert
			}),
			fragment:loadContextConditionalShader(gl,{
				webgl1:frag1,
				webgl2:frag
			}),
			uniforms:[
				"scale",
				"percent",
				"resolution",
				"uViewport"
			]
		});


		// build instanced data
		let uvs = [];

		for(let j = 0; j < numParticles; ++j){
			for(let i = 0; i < numParticles; ++i){
				let x = i / numParticles;
				let y = j / numParticles;
				uvs.push(x,y);
			}
		}

		this.addAttribute('position',model.positions);
		this.addAttribute('uv',model.coords,2);
		this.addInstancedAttribute("aUV",uvs,2);
		this.addIndices(model.indices);
		this.setNumInstances();
	}
}

export default Petals
