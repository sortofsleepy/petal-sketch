import Mesh from '../jirachi/framework/Mesh'
import {range} from '../jirachi/math/core'
import vert from '../shaders/save.vert'
import frag from '../shaders/save.frag'


class SaveView extends Mesh {
	constructor(gl,numParticles){
		super(gl,{
			vertex:vert,
			fragment:frag
		});

		this._build(numParticles);
	}

	_build(numParticles){
		let positions = [];
		let coords = [];
		let indices = [];
		let extras = [];
		let count = 0;


		let totalParticles = numParticles * numParticles;
		console.debug('Total Particles : ', totalParticles);
		let ux, uy;
		let max = 3;

		for(let j = 0; j < numParticles; j++) {
			for(let i = 0; i < numParticles; i++) {
				positions.push([range(-max,max), range(-max,max), range(-max,max)]);

				ux = i / numParticles * 2.0 - 1.0 + .5 / numParticles;
				uy = j / numParticles * 2.0 - 1.0 + .5 / numParticles;

				extras.push([Math.random(), Math.random(), Math.random()]);
				coords.push([ux, uy]);
				indices.push(count);
				count ++;

			}
		}

		this.addAttribute('aVertexPosition',positions);
		this.addAttribute('aTextureCoord',coords,2);
		this.addIndices(indices);
	}
}
export default SaveView;