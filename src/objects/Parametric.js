import Mesh from '../jirachi/framework/Mesh'
import vec2 from '../jirachi/math/vec2'
import createTube from './createTube'
import vert from '../shaders/para.vert'
import frag from '../shaders/para.frag'
import {range} from '../jirachi/math/core'

// adapted from this
// https://mattdesl.svbtle.com/shaping-curves-with-parametric-equations
class Parametric extends Mesh {
	constructor(gl,{
		radius=1,
		length=1,
		numSides=8,
		subdivisions=50,
		openEnded=false
	}={}){
		super(gl,{
			vertex:vert,
			fragment:frag,
			uniforms:[
				'thickness',
				'index',
				'radialSegments'
			]
		});


		let geo = createTube(numSides,subdivisions,openEnded);

		this.thickness = range(0.005,0.0032);
		this.numVertices = geo.positions.length / 3;
		this.addAttribute('position',geo.positions,1);
		this.addAttribute('angle',geo.angles,1);
		this.addAttribute('uv',geo.uvs,2);


	}


}
export default Parametric;